import { Component } from '@angular/core';
import { EmployeeDetailList } from '../employee-service/employee-details.service';
import { EmployeeService } from '../employee-service/employee-service';

@Component(
    {
        selector: 'app-show-employee-details',
        templateUrl: './show-empoyee-details.component.html'
    }
)
export class ShowEmployeeDetails {

    employeeList: EmployeeService[]=[]
    constructor(private employeeDetailList: EmployeeDetailList) {
        this.employeeList = employeeDetailList.getEmployeeDetailsList();
        this.employeeList.forEach(employee => {
            console.log(employee)
        });
        
    }


}