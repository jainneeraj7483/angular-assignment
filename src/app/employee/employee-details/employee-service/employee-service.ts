export class EmployeeService {
    id: String
    firstName: String
    lastName: String
    dateOfJoining: Date

    constructor(id, firstName, lastName, dateOfJoining) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
        this.dateOfJoining = dateOfJoining
    }

}