import { EmployeeService } from './employee-service';

export class EmployeeDetailList {

    constructor() {
    }

    employeeList: EmployeeService[] = []
    counter = 1
    addEmployee() {
        this.employeeList.push(new EmployeeService(this.counter, 'employeeFirstName' + this.counter,
            'employeeLastName' + this.counter, new Date()))
        this.counter = this.counter + 1;
    }

    getEmployeeDetailsList() {
        return this.employeeList;
    }

}