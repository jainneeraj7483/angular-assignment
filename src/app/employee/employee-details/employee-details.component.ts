import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit() {
  }

  onShowEmployee() {
    this.router.navigate(['show'], { relativeTo: this.route });
  }
  onAddEmployee() {
    this.router.navigate(['add'], { relativeTo: this.route });
  }

}
