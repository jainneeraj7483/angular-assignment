import { Component } from '@angular/core';
import { EmployeeDetailList } from '../employee-service/employee-details.service';

@Component(
    {
        selector: 'app-add-employee-details',
        templateUrl: './add-employee-details.component.html'
    }
)
export class AddEmployeeDetails {
    totalEmployee
    constructor(private employeeList: EmployeeDetailList) {
        this.totalEmployee = this.employeeList.getEmployeeDetailsList().length;
    }

    onAddEmployee() {
        this.employeeList.addEmployee();
        this.totalEmployee = this.employeeList.getEmployeeDetailsList().length;
    }
}