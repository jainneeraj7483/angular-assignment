import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeDetailsComponent } from './employee/employee-details/employee-details.component';
import { AddEmployeeDetails } from './employee/employee-details/add-employee-details/add-employee-details.component';
import { ShowEmployeeDetails } from './employee/employee-details/show-employee-details/show-employee-details.component';
import { Route } from '@angular/compiler/src/core';
import { EmployeeService } from './employee/employee-details/employee-service/employee-service';
import { EmployeeDetailList } from './employee/employee-details/employee-service/employee-details.service';

const employeeRoutes: Routes = [
  {
    path: '',
    redirectTo: 'employee-details',
    pathMatch: 'full'
  },
  {
    path: 'employee-details',
    component: EmployeeDetailsComponent,
    children: [
      {
        path: 'add',
        component: AddEmployeeDetails
      },
      {
        path: 'show',
        component: ShowEmployeeDetails
      },
    ]
  }
];
@NgModule({
  declarations: [
    AppComponent,
    EmployeeDetailsComponent,
    AddEmployeeDetails,
    ShowEmployeeDetails
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(employeeRoutes)
  ],
  providers: [EmployeeDetailList],
  bootstrap: [AppComponent]
})
export class AppModule { }
